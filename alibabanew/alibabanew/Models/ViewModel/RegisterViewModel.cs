﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alibabanew.Models.ViewModel
{
    public class RegisterViewModel
    {
        public string userName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phoneNumber { get; set; }
    }
}
