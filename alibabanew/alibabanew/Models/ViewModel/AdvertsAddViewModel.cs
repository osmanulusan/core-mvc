﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace alibabanew.Models.ViewModel
{
    public class AdvertsAddViewModel
    {
        [StringLength(200), Required(ErrorMessage = "Başlık alanı doldurulmalıdır")]
        public string Title { get; set; }

        [DataType(DataType.Html)]
        [Required(ErrorMessage = "Açıklama alanı doldurulmalıdır")]
        public string Description { get; set; }

        public bool Warranty { get; set; } = false;

        [Required]
        public int ColorId { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public int SubCategoryId { get; set; }

        [Required]
        public int TrademarkId { get; set; }

        [Required]
        public int ModelId { get; set; }

        [Required]
        public string Image1 { get; set; }

        public string Image2 { get; set; }

        public string Image3 { get; set; }
    }
}
