﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using alibabanew.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using MyApi.Model;

namespace alibabanew.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        //public async Task<IActionResult> Index(Adverts adverts)
        //{
        //    Adverts ilanCek = new Adverts();
        //    using (var httpClient = new HttpClient())
        //    {
        //        httpClient.DefaultRequestHeaders.Add("Admin", "Administrator@123");
        //        StringContent content = new StringContent(JsonConvert.SerializeObject(adverts), Encoding.UTF8, "application/json");

        //        using (var response = await httpClient.PostAsync("https://localhost:44363/api/Auth/IlanCek", content))
        //        {
        //            string apiResponse = await response.Content.ReadAsStringAsync();
        //            try
        //            {
        //                ilanCek = JsonConvert.DeserializeObject<Adverts>(apiResponse);//gavat
        //            }
        //            catch (Exception ex)
        //            {
        //                ViewBag.Result = apiResponse;
        //                return View();
        //            }
        //        }
        //    }
        //    return View(ilanCek);
        //}
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult IlanEkle()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> IlanEkle(Ilan ilan)
        {
            Ilan ilanEkle = new Ilan();
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("Admin", "Administrator@123");
                StringContent content = new StringContent(JsonConvert.SerializeObject(ilan), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync("https://localhost:44363/api/Auth/Ilan", content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    try
                    {
                        ilanEkle = JsonConvert.DeserializeObject<Ilan>(apiResponse);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Result = apiResponse;
                        return View();
                    }
                }
            }
            return View(ilanEkle);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
