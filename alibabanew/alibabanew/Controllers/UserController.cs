﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using alibabanew.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using MyApi.Model;
using Newtonsoft.Json;

namespace alibabanew.Controllers
{
    public class UserController : Controller
    {

        private readonly ILogger<UserController> _logger;

        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AdvertsAdd()
        {
            return View();
            //deneme
        }

        [HttpPost]
        public async Task<IActionResult> AdvertsAdd(AdvertsAddViewModel adverts)
        {
            AdvertsAddViewModel advertsAdd = new AdvertsAddViewModel();
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(adverts), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync("https://localhost:44363/api/User/AddAdvert", content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    try
                    {
                        advertsAdd = JsonConvert.DeserializeObject<AdvertsAddViewModel>(apiResponse);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Result = apiResponse;
                        return View();
                    }
                }
            }
            return View(advertsAdd);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel RegisterModel)
        {
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(RegisterModel), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync("https://localhost:44363/api/Auth/Register", content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    try
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.OK && apiResponse == "true")
                        {
                            return Json("Kayıt Tamamlandı");
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Result = apiResponse;
                        return Json("Hata");
                    }
                }
            }
            return View();
        }
    }
}